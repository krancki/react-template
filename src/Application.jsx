import React from "react"

class Application extends React.Component{
    
    constructor(props){
        super(props);

        
        this.state = {
            people : [],
            appContainer: props.config.appContainer,
            btnAdd : props.config.btnAdd,
            fileName : props.config.firstName,
            lastName: props.config.lastName,
            phoneNumber: props.config.phoneNumber
        }
   
        this.addPerson = this.addPerson.bind(this);
        this.generateTable = this.generateTable.bind(this);
        this.removePerson = this.removePerson.bind(this);
    }

    
    

    addPerson() {
        
        let people = this.state.people;

        const firstNameValue = firstName.value;
        const lastNameValue = lastName.value;
        const phoneNumberValue = phoneNumber.value;
        firstName.value="";
        lastName.value="";
        phoneNumber.value="";
        let id = 0;
        
        if (people.length > 0) {
            id = people[people.length - 1].id + 1;
        }

        people.push({
            id: id, firstName: firstNameValue, lastName: lastNameValue, phoneNumber: phoneNumberValue
        })
        
        this.setState({...this.state, people});
        
    }
    

//      updatePerson= (sender) =>{
//        let id = sender.target.getAttribute('data-id');
        
        
//         const firstNameValue = firstName.value;
//         const lastNameValue = lastName.value;
//         const phoneNumberValue = phoneNumber.value;
       
        
//         this.people[id]={
//             firstName: firstNameValue, lastName: lastNameValue, phoneNumber: phoneNumberValue
//         };
//         this.render();
//          const addPersonBtn =document.getElementById("btn-add");
//          document.getElementById(this.btnAdd).onclick = this.addPerson;
//          addPersonBtn.innerHTML="Add person";
//         firstName.value="";
//         lastName.value="";
//         phoneNumber.value="";
//     }

    removePerson (sender){
    
        const id = sender.target.dataset["id"];
        let people = this.state.people;

        const index=people.findIndex((element)=>{
            return id==element.id;
        })

        people.splice(index,1);
        this.setState({...this.state,people});
    }

    generateTable(people){
        
        return people.map((element,key)=>(
            <tr key={key}>
            <td>{element.firstName}</td>
            <td>{element.lastName}</td>
            <td>{element.phoneNumber}</td>
            <td><button className="btn-remove" data-id={element.id} onClick={this.removePerson}>Remove</button></td>
            <td><button className="btn-update" data-id={element.id}>Udate</button></td>
            </tr>
        ))
    }

   render(){
       return(
        <div className="container">
        <div id="app-container"></div>
            <div className="row">
                <div className="col-md-3"><input type="text" placeholder="First name" id="firstName" /></div>
                <div className="col-md-3"><input type="text" placeholder="Last name" id="lastName" /></div>
                <div className="col-md-3"><input type="text" placeholder="Phone number" id="phoneNumber" /></div>
                <div className="col-md-3"><button id="btn-add" onClick={this.addPerson} type="button">Add person</button></div>
            </div>
        <table className="table">
        <thead>
            <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone number</th>
            <th></th>
            </tr>
            </thead> 
            <tbody>
        {this.generateTable(this.state.people)}
        </tbody>
        </table>
        </div>
       )
        
       
   }




    //     const container = document.getElementById(this.appContainer);
    //     let html = '<table class="table">' +
    //         '<thead><tr><th>First Name</th><th>Last Name</th><th>Phone number</th><th></th></tr></thead' +
    //         '<tbody>';

    //     for (let index = 0; index < this.people.length; index++) {
    //         const person = this.people[index];
    //         html += '<tr>' +
    //             '<td>' + person.firstName + '</td>' +
    //             '<td>' + person.lastName + '</td>' +
    //             '<td>' + person.phoneNumber + '</td>' +
    //             '<td><button class="btn-remove" data-id="' + person.id + '">Remove</button></td>' +
    //             '<td><button class="btn-update" data-id="' + person.id + '">Udate</button></td>' +
    //             '</tr>';
    //     }

    //     html += '</tbody></table>'
    //     container.innerHTML = html;

    //     const buttons = document.getElementsByClassName("btn-remove");
    //     for (let index = 0; index < buttons.length; index++) {
    //         const element = buttons[index];
    //         element.onclick = this.removePerson;
    //     }
       
    //     const updateButtons = document.getElementsByClassName("btn-update");
    //    for (let index = 0; index < updateButtons.length; index++) {
    //         const element = updateButtons[index];
    //         element.onclick = this.clearField;
    //     }
       
    // }

//     run() {
//         this.render();
//         document.getElementById(this.btnAdd).onclick = this.addPerson;
//     }

}
    
    
 export default Application