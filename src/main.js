import React from 'react';
import {config} from './config-class'
import Application from './Application'

class Main extends React.Component {

    constructor(props) {
        super(props);

    }



    render() {
        return (
            
            <Application config={config}/>
        )
    } 
}

export default Main;